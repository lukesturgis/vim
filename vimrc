set encoding=UTF-8

"Start NERDTree and leave the cursor in it.
autocmd VimEnter * NERDTree | wincmd p

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'scrooloose/syntastic'
Plug 'vim-airline/vim-airline'
Plug 'janko-m/vim-test'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'klen/python-mode'
Plug 'junegunn/goyo.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'davidhalter/jedi-vim'
Plug 'elzr/vim-json'
Plug 'severin-lemaignan/vim-minimap'
Plug 'junegunn/limelight.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'lepture/vim-jinja'
Plug 'lervag/vimtex'
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" 9000+ Snippets
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

" lua & third party sources -- See https://github.com/ms-jpq/coq.thirdparty
" Need to **configure separately**

Plug 'ms-jpq/coq.thirdparty', {'branch': '3p'}
call plug#end()

autocmd vimenter * ++nested colorscheme gruvbox
set background=dark

set splitright

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" these "Ctrl mappings" work well when Caps Lock is mapped to Ctrl
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

" Open up nerdtree and a bottom terminal
function In()
  execute "below terminal ls ++rows=15"
  execute "NERDTreeToggle"
  execute "wincmd l"
  execute "AirlineTheme zenburn"
endfunction
command! In call In()


let NERDTreeShowHidden=1


" vim-powered terminal in split window
map <Leader>t :below term++rows=15 tmux<cr>
tmap <Leader>t <c-w>:term ++close <cr>

map <Leader>g <c-w>:Goyo<cr>

" vim-powered terminal in new tab
map <Leader>T :tab term ++close<cr>
tmap <Leader>T <c-w>:tab term ++close<cr>

nmap <Leader>f <Plug>FocusModeToggle<cr>
let g:focusmode_width = 72
let g:airline_powerline_fonts = 1
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240


set number

inoremap jk <ESC>

set hlsearch " highlight all results
set ignorecase " ignore case in search
set incsearch " show search results as you typeset mouse=a






" This is necessary for VimTeX to load properly. The "indent" is optional.
" Note that most plugin managers will do this automatically.
filetype plugin indent on

" This enables Vim's and neovim's syntax-related features. Without this, some
" VimTeX features will not work (see ":help vimtex-requirements" for more
" info).
syntax enable

" Viewer options: One may configure the viewer either by specifying a built-in
" viewer method:
let g:vimtex_view_method = 'zathura'

" Or with a generic interface:
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'

" VimTeX uses latexmk as the default compiler backend. If you use it, which is
" strongly recommended, you probably don't need to configure anything. If you
" want another compiler backend, you can change it as follows. The list of
" supported backends and further explanation is provided in the documentation,
" see ":help vimtex-compiler".
let g:vimtex_compiler_method = 'latexrun'

" Most VimTeX mappings rely on localleader and this can be changed with the
" following line. The default is usually fine and is the symbol "\".
let maplocalleader = ","
